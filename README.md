# GC Media Folders

GC Media Folders is a [WordPress](https://wordpress.org) plugin that enables a more user-friendly folder structure for your media library. By default, WordPress will save any new media assets in folders that go by year and month (i.e. `/wp-content/uploads/2019/01/your-awesome-file.pdf`). However, if you manage a quite large website, you might want to have your files organized in a way that mimics your site tree, so that it becomes easier to understand what page a given file belongs to. Our plugin implements this feature in a very simple and transparent way. GC Media Folders is proudly maintained by the Office of Communications and Marketing at [The Graduate Center, CUNY](https://www.gc.cuny.edu/) and is released under the [MIT License](LICENSE).

## Getting Started

You can install this plugin either by [downloading the zip file](https://gitlab.com/gccomms/gc-media-folders/-/archive/master/gc-media-folders-master.zip) or via command line by cloning the repository under **wp-content/plugins**:

`git clone -b master https://gitlab.com/gccomms/gc-media-folders.git /var/www/html/wp-content/plugins/gc-media-folders/`

GC Media Folders will only work if you have nice permalinks enabled in WordPress, or in other words if your URLs look like https://example.com/about/team/. After the plugin has been activated, test it by editing a page on your website and uploading a new media assets there. You will see that the path where your media asset has been saved reflects the permalink for that page: https://example.com/wp-content/uploads/media/about/team/identity.pdf.