<?php
/**
 * Plugin Name: GC Media Folders
 * Description: Change the media library upload folder organization to reflect the site's permalink structure
 * Version: 1.0
 * Author: The Graduate Center, Office of Communications and Marketing
 * License: GPL2
 */

class gc_media_folders{
	public static $post_id = 0;
	public static $file_extension = '';

    public static function init(){
			add_filter( 'wp_handle_upload_prefilter', array( __CLASS__, 'upload_prefilter' ) );	
			add_action( 'xmlrpc_call', array( __CLASS__, 'xmlrpc_call' ) );
			add_filter( 'wp_handle_upload', array( __CLASS__, 'remove_upload_handle' ) );	
    }

    public static function xmlrpc_call( $_call ){
		if( $_call !== 'metaWeblog.newMediaObject' ) {
			return false;
		}		
		
		$data = $GLOBALS[ 'wp_xmlrpc_server' ]->message->params[ 3 ];

		if( !empty( $data[ 'post_id' ] ) ){
			self::$post_id = ( int ) $data[ 'post_id' ];
		}

		self::upload_prefilter( $data );
	}

	public static function upload_prefilter( $_file ){
		if( !empty( $_file[ 'name' ] ) ){
			$wp_filetype = wp_check_filetype( $_file[ 'name' ] ); 
			self::$file_extension = !empty( $wp_filetype[ 'ext' ] ) ? $wp_filetype[ 'ext' ] : '';
		}
		add_filter( 'upload_dir', array( __CLASS__, 'custom_upload_dir' ) );
		return $_file;
	}

	public static function custom_upload_dir( $_path ){		
		$my_post_id = ( !empty( $GLOBALS[ 'post' ]->ID ) ? $GLOBALS[ 'post' ]->ID : ( !empty( $_REQUEST[ 'post_id' ] ) ? $_REQUEST[ 'post_id' ] : ( !empty( self::$post_id ) ? self::$post_id : 0 ) ) );

		if( !empty( $_path[ 'error' ] ) || get_post_type( $my_post_id ) == 'post' ) {
			return $_path;
		}

		$customdir = self::generate_dir( $my_post_id );

		$_path[ 'path' ] = str_replace( $_path[ 'subdir' ], '', $_path[ 'path' ] ); //remove default subdir (year/month)
		$_path[ 'url' ] = str_replace( $_path[ 'subdir' ], '', $_path[ 'url' ] );
		$_path[ 'subdir' ] = $customdir;
		$_path[ 'path' ] .= $customdir;
		$_path[ 'url' ] .= $customdir;

		return $_path;
	}

	public static function generate_dir( $_post_id = 0 ) {	
		if ( empty( $_post_id ) ) {
			return '/media/global-assets';
		}
		else{
			list( $permalink, $postname ) = get_sample_permalink( $_post_id );
			$page_asset_folder = untrailingslashit( wp_make_link_relative( str_replace( get_option( 'home' ), '', str_replace( array( '%postname%', '%pagename%' ), $postname, $permalink ) ) ) );

			if ( empty( $page_asset_folder ) ) {
				$page_asset_folder = '/media/global-assets';
			}
			return '/media' . $page_asset_folder;
		}
	}

	public static function remove_upload_handle( $_fileinfo ){	
		remove_filter( 'upload_dir', array( __CLASS__, 'upload_dir' ) );
		return $_fileinfo;
	}
}
add_action( 'plugins_loaded', array( 'gc_media_folders', 'init' ) );